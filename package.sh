#!/bin/bash
git diff --name-status develop -- > diffBranches.txt

readarray filelist < <(sed 's/\t/\n/g' diffBranches.txt)
mkdir -p release
declare -A packagearray
declare -A metadatamap
status=""

file  ./metadataMapping
while IFS=' ' read value name
do
    metadatamap[$value]="$name"
done < $"./metadataMapping"

for i in ${!filelist[@]}
do
filepath="$(echo -e "${filelist[i]}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

case "${filepath}" in
	M)
	status="$filepath"
	;;
	A)
	status="$filepath"
	;;
	D)
	status="$filepath"
	;;
	R???)
	status="R"
	#  a file renamed
	;;
	C???)
	status="C"
	#  a file copied
	;;
	*)
		#filepath
		filename="$(basename -- "${filepath}")"
		echo filename "$filename"
		filenameonly="$(cut -d '.' -f1 <<<"${filename}")"
		echo  filenameonly "$filenameonly"
		filetype="$(cut -d/ -f4 <<<"${filepath}")"
		echo filetype "$filetype"
		fileparent="$(cut -d/ -f5 <<<"${filepath}")"
		echo fileparent "$fileparent"
		if [[ "$filetype" =~ ^(aura|certs|classes|components|contentassets|email|pages|scontrols|triggers)$ ]]; then
		fileext="${filename##*.}"
		echo  fileext "$fileext"
		else fileextmeta="$(cut -d '.' -f2 <<<"${filename}")"
		fileext="$(cut -d '-' -f1 <<<"${fileextmeta}")"
		echo  fileext "$fileext"
		fi
		filefolder=$(basename $(dirname "${filepath}"))
		echo  filefolder "$filefolder"
		echo  filepath "$filepath"

		case "${status}" in
			R)
			;;
			C)
			;;
			D)
			;;
			*)
				if [[ "$filename" != "package.xml" ]] && [[ "$filename" != "metadataMapping" ]] && [[ "$filename" != "package.sh" ]] && [[ "$filename" != "bitbucket-pipelines.yml" ]]; then

					mkdir -p release"/"$(dirname "${filepath}") && cp -p "${filepath}" "release/${filepath}"
					if [[ "$fileext" == "cls" ]] || [[ "$fileext" == "trigger" ]] || [[ "$fileext" == "asset" ]] || [[ "$fileext" == "scf" ]] || [[ "$fileext" == "js" ]] || [[ "$fileext" == "page" ]] || [[ "$fileext" == "component" ]] || [[ "$fileext" == "crt" ]] || [[ "$fileext" == "cmp" ]] || [[ "$fileext" == "email" ]]; then
						if [[ "$filetype" != "aura" ]] && [[ "$fileext" != "js" ]]; then
							cp -p "${filepath}""-meta.xml" "release/${filepath}""-meta.xml"
						else cp -p "${filepath}" "release/${filepath}"
						fi
					fi
					if [[ "$filetype" == "aura" || "$filetype" == "lwc" ]]; then
						packagemember=$filefolder
						completefolder=$filefolder
                        cp -r force-app/main/default/$filetype/$completefolder "release/force-app/main/default/$filetype/"
					fi
				fi
			;;
		esac
	;;
esac

done