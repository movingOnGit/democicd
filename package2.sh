#!/bin/bash

declare -A metadatamap
file  ./metadataMapping
while IFS=' ' read value name
do
    metadatamap[$value]="$name"
    echo metadatamap[$value]="$name"
done < $"./metadataMapping"

git diff --name-status develop -- > difffile.txt

readarray filelist < <(sed 's/\t/\n/g' difffile.txt)
mkdir -p release

declare -A packagearray

GitStatus=""

for i in ${!filelist[@]}
do	
filepath="$(echo -e "${filelist[i]}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

case "${filepath}" in
	M) 
	GitStatus="$filepath"
	;;
	A) 
	GitStatus="$filepath"
	;;
	D) 
	GitStatus="$filepath"
	;;
	R???) 
	GitStatus="R"
	;;
	C???)
	GitStatus="C"
	;;
	*)	
		filename="$(basename -- "${filepath}")"
		fileext="${filename##*.}"
		filenameonly="${filename%.*}"
		filetype="$(cut -d/ -f2 <<<"${filepath}")"
		filefolder=$(basename $(dirname "${filepath}")) 
		
		case "${GitStatus}" in	
			R) 
			;;
			C) 
			;;
			D) 			
			;;
			*)	
				if [[ "$filename" != "package.xml" ]]; then
				
					mkdir -p release"/"$(dirname "${filepath}") && cp -p "${filepath}" "release/${filepath}"

					if [[ "$fileext" == "cls" ]] || [[ "$fileext" == "trigger" ]] || [[ "$fileext" == "js" ]] || [[ "$fileext" == "cmp" ]] || [[ "$fileext" == "email" ]]; then
						cp -p "${filepath}""-meta.xml" "release/${filepath}""-meta.xml"
					fi
					if [[ ${filepath} == *"-meta.xml"* ]]; then
						metafilepath=${filepath%-*}
                        filename="${filename%.*}"
						filename=${filename%-*}
                       cp -p "${metafilepath}" "release/${metafilepath}" 
					fi	
					
					packagemember=$filename
					if [[ "$filetype" == "aura" || "$filetype" == "lwc" ]]; then
						packagemember=$filefolder
						completefolder=$(dirname "${file}")
                        cp -r ./$completefolder "./release/mdapi/$filetype/"
					fi
					if [[ "$fileext" == "report" ]] || [[ "$filetype" == "email" ]] || [[ "$fileext" == "dashboard" ]]; then
						packagemember=$folder/""$filename
					fi
					
					packageitems=${packagearray["$filetype"]};
					if [[ "$packageitems" =~ *"<members>"$packagemember"</members>"* ]]; then
						echo "This item is already in the package"
					else
						packagearray["$filetype"]+="<members>"$packagemember"</members>"
					fi
				fi
			;;
		esac
	;;
esac

done	


Package="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Package xmlns=\"http://soap.sforce.com/2006/04/metadata\">"

for member in "${!packagearray[@]}"; do   
	Package+="<types>${packagearray["$member"]}<name>${metadatamap["$member"]}</name></types>" 
done

Package+="<version>47.0</version></Package>"

echo $Package > release/mdapi/Package.xml
