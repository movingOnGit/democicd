FROM alpine

#add usefull tools
RUN apk add --update --no-cache  \
      && apk add openssl \
      git \
      findutils \
      bash \
      unzip \
      curl \
      wget \
      nodejs \
      nodejs-npm \
      openjdk8-jre \
      openssh-client \
      perl \
      jq

# install sfdx from npm
RUN npm install sfdx-cli --global
RUN sfdx --version
RUN sfdx plugins
